//LIBRERIS EXTERNAS
import React,{Component} from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';

//COMPONENTES
import Title from './components/Title';
//HOJAS DE ESTILOS
import './App.css';



class App extends Component {

  render(){
    return (
      <MuiThemeProvider>
        <div className="App-background">
          <div className="Header-main">
            <Title/>
            <RaisedButton label="Crear cuenta gratuita" primary={true}/>
            <img className="Header-illustration" src={ process.env.PUBLIC_URL + '/images/236641-699x450-etapas-desarrollo-gatitos.jpg' }/>
          </div>
          <div>
            <ul>
              <li>
                <h3>Calificaciones con emociones</h3>
                <p>Califica tus lugares con experiencias, no con números</p>
              </li>
              <li>
                <h3>¿Sin Internet? Sin problemas</h3>
                <p>Places funciona sin internet o conexiones lentas</p>
              </li>
              <li>
                <h3>Tus lugares favoritos</h3>
                <p>Define tu lista de sitios favoritos</p>
              </li>
            </ul>
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
