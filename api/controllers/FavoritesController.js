const buildParams = require('./helpers').buildParams;

const valiParams = ['_place'];

const FavoritePlace = require('../models/FavoritePlace');
const User = require('../models/User');

function find(req,res,next){
    FavoritePlace.findById(req.params.id).then(fav=>{
        req.favorite = fav;
        req.mainObj = fav;
        next();
    }).catch(err=>{
        next(err);
    });
}

function index(req,res)
{
    if(!req.fullUser) return res.json('No user found');

    req.fullUser.favorites.then(places=>{
            res.json(places);
    }).catch(err=>{
        console.log(err);
        res.json(err);
    });
}

function create(req,res){

    let params = buildParams(valiParams, req.body);
    params['_user'] = req.user.id; //y aqui llega
    
    console.log('params[\'_user\'] = ' + params['_user']);
    console.log('params[\'_place\'] = ' + params['_place']);

    FavoritePlace.create(params)
        .then(favorite=>{
            res.json(favorite);
        }).catch(err=>{
            res.status(422).json({err});
        });
}

function destroy(req,res){
    req.favorite.remove().then(doc=>{
        res.json({'Eliminación': 'exitosa'});
      })
        .catch(err=>{
            res.status(500).json({err});
      });
}

module.exports = {index, create, destroy, find}
