const buildParams = require('./helpers').buildParams;

const valiParams = ['_place', 'reactions', 'observation'];

const Visit = require('../models/Visit');
const User = require('../models/User');

function find(req,res,next){
    Visit.findById(req.params.visit_id).then(visit=>{
        req.visit = visit;
        req.mainObj = visit;
        next();
    }).catch(err=>{
        next(err);
    });
}

function index(req,res){

    let promise = null;

    if(req.place){
        promise = req.place.visits; // si quiero ver las visitas hechas de un lugar
    }else{
        if(req.user){
            promise = Visit.forUser(req.user.id, req.query.page || 1); // si quiero ver las visitas d eun usuario concreto
        }
    }

    if(promise){
        promise.then(visits=>{
            res.json(visits);
        }).catch(error=>{
            res.status(500).json({error});
        });
    }else{
        res.status(404).json({"Error": "no hay promesa :'( "});
    }
}

function create(req,res){

    let params = buildParams(valiParams, req.body);
    params['_user'] = req.user.id; //y aqui llega
    
    // console.log('params[\'_user\'] = ' + params['_user']);
    // console.log('params[\'_place\'] = ' + params['_place']);

    Visit.create(params)
        .then(visit=>{
            res.json(visit);
        }).catch(err=>{
            res.status(422).json({err});
        });
}

function destroy(req,res){
    req.visit.remove().then(doc=>{
        res.json({'Eliminación': 'exitosa'});
      })
        .catch(err=>{
            res.status(500).json({err});
      });
}

module.exports = {index, create, destroy, find}
