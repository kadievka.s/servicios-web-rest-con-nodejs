const Place = require('../models/Place');
const upload = require('../config/upload');//esto hace que la imagen la suba un usuario en un form y se guarde en la carpeta updates
const uploader = require('../models/Uploader');
const helpers = require('./helpers');

const valiParams = ['title', 'description', 'acceptsCreditCard', 'openHour', 'closeHour', 'address'];

function find(req,res,next){
    Place.findOne({
        slug: req.params.id
    })
    .then(place=>{
        req.place = place;
        req.mainObj = place;
        next();
    }).catch(err=>{
        next(err);
    });
}

function index(req,res){
    Place.paginate({}, {
        page: req.query.page || 1,
        limit: 8,
        sort: {
            '_id': -1
        }
    }).then(doc=>{
        res.json(doc);
      })
        .catch(err=>{
          console.log(err);
          res.json(err);
      });
}

function create(req,res, next){

    const params = helpers.buildParams(valiParams, req.body);
    params['_user'] = req.user.id; //y aqui llega

    Place.create(params).then(doc=>{
            req.place = doc;
            next();
          })
        .catch(err=>{
            next(err);
        });
}

function show(req,res){
    res.json(req.place);
}

function update(req,res){
    const params = helpers.buildParams(valiParams, req.body);

    req.place = Object.assign(req.place, params); //es inseguro porque no posee un filtro de campos
    req.place.save().then(doc=>{
        res.json(doc);
    }).catch(err=>{
        console.log(err);
        res.json(err);
    });
}

function destroy(req,res){
    req.place.remove().then(doc=>{
        res.json({});
      })
        .catch(err=>{
          console.log(err);
          res.json(err);
      });
}

function multerMiddleware(){
    return upload.fields([//este recibe los campos de imagen llamados avatar y cover y los sube a la carpeta upload
        {name: 'avatar', maxCount: 1},
        {name: 'cover', maxCount: 1},
    ]);
}

function saveImage(req,res){//luego que se crea un registro Place en la BD se guarda la imagen en la nube
    
    const imageTypes = ['avatar', 'cover'];

    const promises = [];
    
    imageTypes.forEach(imageType=>{
        if(req.place){//el recibe el objeto req con la propiedad doc
            if(req.files && req.files[imageType]){
                const path = req.files[imageType][0].path; //obtener el lugar donde esta almacenada la imagen
                promises.push(req.place.updateImage(path, imageType));
            }
        }else{// en caso que no haya imagen
            res.status(422).json({
                error: req.error || 'Could not save place'
            });
        }
    });

    Promise.all(promises).then(results=>{
        console.log(req.place);
        res.json(req.place);
    }).catch(err=>{
        console.log(err);
        res.json(err);
    });
    
}

module.exports = {index, create, show, update, destroy, find, multerMiddleware, saveImage}