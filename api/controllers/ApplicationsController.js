const buildParams = require('./helpers').buildParams;

const valiParams = ['origin', 'name'];

const Application = require('../models/Application');
const User = require('../models/User');

function find(req,res,next){
    Application.findById(req.params.id).then(application=>{
        req.application = application;
        req.mainObj = application;
        next();
    }).catch(err=>{
        next(err);
    });
}

function index(req,res){

    
}

function create(req,res){

    let params = buildParams(valiParams, req.body);

    Application.create(params)
        .then(application=>{
            res.json(application);
        }).catch(err=>{
            res.status(422).json({err});
        });
}

function destroy(req,res){
    req.application.remove().then(doc=>{
        res.json({'Eliminación': 'exitosa'});
      })
        .catch(err=>{
            res.status(500).json({err});
      });
}

module.exports = {index, create, destroy, find}
