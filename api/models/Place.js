const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const uploader = require('./Uploader');
const slugify = require('../plugins/slugify');

const Visit = require('./Visit');

let placeSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    slug:{
        type: String,
        unique: true,

    },
    description: String,
    address: String,
    acceptsCreditCard: {
        type: Boolean,
        default: false
    },
    coverImage: String,
    avatarImage: String,
    openHour: Number,
    closeHour: Number,
    _user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true

    }
});

placeSchema.methods.updateImage = function (path, imageType){
    return uploader(path).then(secure_url => this.saveImageUrl(secure_url, imageType));
}

placeSchema.methods.saveImageUrl = function(secureUrl, imageType){
    this[imageType + 'Image'] = secureUrl;
    return this.save();
}

placeSchema.pre('save', function(next){
    if(this.slug) next();

    generateSlugAndContinue.call(this, 0, next); //call(this, es para pasar el objeto de aqui para otra funcion
    next();
});

placeSchema.statics.isRepeated = function (slug){
    return Place.count({slug: slug}).then(count=>{
        if(count > 0) {
            return true;
        }else{
            return false;
        }
    });
}

function generateSlugAndContinue(count, next){

    this.slug = slugify(this.title); //genera un slug

    if(count != 0){
        this.slug = this.slug + "-" +count;
    }

    Place.isRepeated(this.slug).then(itIs=>{//si esta repetido
        if(itIs)
        return generateSlugAndContinue.call(this, count+1, next); //suma uno a la cuenta 

        next();
    });
}

placeSchema.virtual('visits').get(function(){
    return Visit.find({'_place': this._id}).sort('-id');
});


placeSchema.plugin(mongoosePaginate);

let Place = mongoose.model('Place', placeSchema);

module.exports = Place;