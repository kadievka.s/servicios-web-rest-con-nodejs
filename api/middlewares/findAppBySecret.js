const Application = require('../models/Application');

module.exports = function(req,res,next)
{
    if(req.xhr) return next();//si esta petición es ajax se ignora

    const secret = req.headers.secret;//esperamos que en el encabezado se envíe un secreto

    if(!secret) return next();//si no hay nada en el encabezado enviamos un error

    Application.findOne({secret}).then(app=>{//si hay un secreto, lo buscamos como atributo de una aplicación

        if(!app) return next(new Error('Invalid application'));//si la app no existe retornamos un error de secreto invalido
    
        req.application = app; //si es válida se guarda en el objeto req.application y continúa la ejecución
        req.validRequest = true;

        next();

    }).catch(err=>{
        next(new Error(err));
    });
}