module.exports = function(options)
{
    let CORsMiddleware = function(req,res,next)
    {
        if(req.application){
            req.application.origin.split(",").forEach(origin => {
                res.header("Acces-Control-Allow-Origin", origin);
            });
            
        }else{
            res.header("Acces-Control-Allow-Origin", '*');
        }

        res.header("Acces-Control-Allow-Origin", "Origin, X-Requested-With, Content-Type, Accept, Authorization, application");

        next();
    }

    CORsMiddleware.unless = require('express-unless');

    return CORsMiddleware;

}