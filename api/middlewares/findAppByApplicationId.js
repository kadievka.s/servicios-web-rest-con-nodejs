const Application = require('../models/Application');

module.exports = function(req,res,next)
{
    if(req.application) return next();//si hay req.application se ignora

    const applicationId = req.headers.application;//esperamos que en el encabezado se envíe applicationId

    if(!applicationId) return next();//si no hay nada en el encabezado enviamos un error

    Application.findOne({applicationId}).then(app=>{//si hay un applicationId, lo buscamos como atributo de una aplicación

        if(!app) return next(new Error('Invalid application'));//si la app no existe retornamos un error de secreto invalido
    
        req.application = app; //si es válida se guarda en el objeto req.application y continúa la ejecución
        
        req.validRequest = req.application.origin.split(",").find(origin=>{
            req.headers.origin = origin.replace(/\s/g,'');
            console.log(req.headers.origin);
            return origin == req.headers.origin;
        });

        next();


    }).catch(err=>{
        next(new Error(err));
    });
}