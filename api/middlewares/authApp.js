const Application = require('../models/Application');

module.exports = function(options){
    let AuthApp = function(req,res,next){

        Application.count({}).then(appCount=>{//verifica si hay aplicaciones en la BD
    
            if(appCount>0 && !req.application) return next(new Error('An application is required to consume this API')); //si no hay nada en la BD ni guardado en req.application ignora todo lo demás
    
            if(!req.validRequest) return next(new Error('Origin Invalid'));
            
            next();
        }).catch(err=>{
            next(err);
        });
    }

    AuthApp.unless = require('express-unless');

    return AuthApp;

}